<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\crm\CrmContactInterface;
use Drupal\crm\CrmUserInterface;
use Drupal\user\UserInterface;

/**
 * Defines the relation entity class.
 *
 * @ContentEntityType(
 *   id = "crm_user",
 *   label = @Translation("CRM User"),
 *   label_collection = @Translation("CRM Users"),
 *   handlers = {
 *     "storage_schema" = "Drupal\crm\UserStorageSchema",
 *     "view_builder" = "Drupal\crm\UserViewBuilder",
 *     "list_builder" = "Drupal\crm\UserListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\crm\Form\UserForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "\Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *   },
 *   base_table = "crm_user",
 *   translatable = FALSE,
 *   admin_permission = "administer crm user",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/crm/user/add",
 *     "canonical" = "/admin/config/crm/user/{crm_user}",
 *     "edit-form" = "/admin/config/crm/user/{crm_user}/edit",
 *     "delete-form" = "/admin/config/crm/user/{crm_user}/delete",
 *     "collection" = "/admin/config/crm/user/list"
 *   }
 * )
 */
class User extends ContentEntityBase implements CrmUserInterface {

  /**
   * {@inheritdoc}
   */
  public function getUser() {
    return $this->get('user')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserId() {
    return $this->get('user')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserId($uid) {
    $this->set('user', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUser(UserInterface $account) {
    $this->set('user', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getContact() {
    return $this->get('crm_contact')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getContactId() {
    return $this->get('crm_contact')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setContactId($individual_id) {
    $this->set('crm_contact', $individual_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setContact(CrmContactInterface $individual) {
    $this->set('crm_contact', $individual->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user'] = BaseFieldDefinition::create('entity_reference')
      ->setRequired(TRUE)
      ->setCardinality(1)
      // @todo Replace with what ever would work with entity reference
      // from core. https://www.drupal.org/project/drupal/issues/2973455
      ->addConstraint('UniqueReference')
      ->setLabel(t('User'))
      ->setDescription(t('The user ID of the relation.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['crm_contact'] = BaseFieldDefinition::create('entity_reference')
      ->setRequired(TRUE)
      ->setCardinality(1)
      // @todo Replace with what ever would work with entity reference
      // from core. https://www.drupal.org/project/drupal/issues/2973455
      ->addConstraint('UniqueReference')
      ->setLabel(t('Individual'))
      ->setDescription(t('The individual ID of the relation.'))
      ->setSetting('target_type', 'crm_contact')
      ->setSetting('handler_settings', ['target_bundles' => ['individual' => 'individual']])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the contact was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the contact was last edited.'));

    return $fields;
  }

}
