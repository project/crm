<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Contact type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "crm_contact_type",
 *   label = @Translation("Contact type"),
 *   label_collection = @Translation("Contact types"),
 *   label_singular = @Translation("contact type"),
 *   label_plural = @Translation("contacts types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count contacts type",
 *     plural = "@count contacts types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\crm\ContactTypeAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\crm\Form\ContactTypeForm",
 *       "edit" = "Drupal\crm\Form\ContactTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\crm\ContactTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer contact types",
 *   bundle_of = "crm_contact",
 *   config_prefix = "crm_contact_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/crm/contact-types/add",
 *     "edit-form" = "/admin/structure/crm/contact-types/manage/{crm_contact_type}",
 *     "delete-form" = "/admin/structure/crm/contact-types/manage/{crm_contact_type}/delete",
 *     "collection" = "/admin/structure/crm/contact-types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "primary",
 *     "locked",
 *   }
 * )
 */
class ContactType extends ConfigEntityBundleBase {

  /**
   * The machine name of this contact type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the contact type.
   *
   * @var string
   */
  protected $label;

  /**
   * The primary relationship type.
   *
   * @var string
   */
  protected $primary;

  /**
   * Whether the contact type is locked.
   *
   * @var bool
   */
  protected $locked = FALSE;

  /**
   * Is the contact type locked?
   */
  public function isLocked() {
    return $this->locked;
  }

}
