<?php

namespace Drupal\crm\Entity;

use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\crm\CrmContactInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the contact entity class.
 *
 * @ContentEntityType(
 *   id = "crm_contact",
 *   label = @Translation("CRM Contact"),
 *   label_collection = @Translation("CRM Contacts"),
 *   label_singular = @Translation("crm contact"),
 *   label_plural = @Translation("contacts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count crm contacts",
 *     plural = "@count crm contacts",
 *   ),
 *   bundle_label = @Translation("Contact type"),
 *   handlers = {
 *     "list_builder" = "Drupal\crm\ContactListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\crm\ContactAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\crm\Form\ContactForm",
 *       "edit" = "Drupal\crm\Form\ContactForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "crm_contact",
 *   revision_table = "crm_contact_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer crm contact types",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "bundle" = "bundle",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "published" = "status",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   revision_metadata_keys = {
 *    "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/crm/contact",
 *     "add-form" = "/crm/contact/add/{crm_contact_type}",
 *     "add-page" = "/crm/contact/add",
 *     "canonical" = "/crm/contact/{crm_contact}",
 *     "edit-form" = "/crm/contact/{crm_contact}/edit",
 *     "delete-form" = "/crm/contact/{crm_contact}/delete",
 *   },
 *   bundle_entity_type = "crm_contact_type",
 *   field_ui_base_route = "entity.crm_contact_type.edit_form",
 * )
 */
class Contact extends EditorialContentEntityBase implements CrmContactInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $this->setNewRevision();

    // If type is individual, set the label field to the name field.
    if ($this->bundle() == 'individual') {
      $formatted_name = NULL;
      $name_array = $this->get('full_name')->getValue();
      if ($name_array != NULL) {
        $name_formatter = \Drupal::service('name.formatter');
        $formatted_name = $name_formatter->formatList($name_array);
        $this->set('name', $formatted_name);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['primary'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel('Primary Contact')
      ->setSetting('target_type', 'crm_contact')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 10,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'before',
        'type' => 'entity_reference',
        'weight' => 0,
      ]);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['sort_name'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Sort Name'))
      ->setSetting('max_length', 255);

    $fields['emails'] = BaseFieldDefinition::create('crm_email')
      ->setLabel(t('Emails'))
      ->setRevisionable(TRUE)
      ->setCardinality(-1)
      ->setDisplayOptions('form', [
        'type' => 'crm_email',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['telephones'] = BaseFieldDefinition::create('crm_telephone')
      ->setLabel(t('Telephones'))
      ->setRevisionable(TRUE)
      ->setCardinality(-1)
      ->setDisplayOptions('form', [
        'type' => 'crm_telephone',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid']
      ->setLabel(t('Owned by'))
      ->setDescription(t('The username of the content author.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status']
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 120,
      ])
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the contact was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the contact was last edited.'));

    return $fields;
  }

}
