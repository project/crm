<?php

namespace Drupal\crm;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

/**
 * Defines the crm_user schema handler.
 */
class UserStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);
    $data_table = 'crm_user';
    if (isset($schema[$data_table])) {

      if (!isset($schema[$data_table]['unique keys'])) {
        $schema[$data_table]['unique keys'] = [];
      }
      $schema[$data_table]['unique keys'] += [
        'crm_user__user' => ['user'],
        'crm_user__crm_contact' => ['crm_contact'],
      ];
      $schema[$data_table]['fields']['user']['not null'] = TRUE;
      $schema[$data_table]['fields']['crm_contact']['not null'] = TRUE;
    }

    return $schema;
  }

}
