<?php

namespace Drupal\crm;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface defining a crm relationship entity type.
 */
interface CrmRelationshipInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

}
