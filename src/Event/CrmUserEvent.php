<?php

namespace Drupal\crm\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\crm\CrmUserInterface;

/**
 * Event that is fired when a user is created, and auto create is enabled.
 */
class CrmUserEvent extends Event {

  /**
   * The event name.
   *
   * @var string
   */
  const EVENT_NAME = 'crm_user_event';


  /**
   * The crm user.
   *
   * @var \Drupal\crm\CrmUserInterface
   */
  protected $crmUser;

  /**
   * Constructs a new CrmUserEvent.
   *
   * @param \Drupal\crm\CrmUserInterface $crm_user
   *   The crm user.
   */
  public function __construct(CrmUserInterface $crm_user) {
    $this->crmUser = $crm_user;
  }

  /**
   * Get the crm user.
   *
   * @return \Drupal\crm\CrmUserInterface
   *   The crm user.
   */
  public function getCrmUser(): CrmUserInterface {
    return $this->crmUser;
  }

  /**
   * Set the crm user.
   *
   * @param \Drupal\crm\CrmUserInterface $crm_user
   *   The crm user.
   */
  public function setCrmUser(CrmUserInterface $crm_user): void {
    $this->crmUser = $crm_user;
  }

}
