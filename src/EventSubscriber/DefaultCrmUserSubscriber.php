<?php

namespace Drupal\crm\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\crm\Event\CrmUserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to crm user create events.
 */
class DefaultCrmUserSubscriber implements EventSubscriberInterface {

  /**
   * The crm contact storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The crm user settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Constructs a new DefaultCrmUserSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->storage = $entity_type_manager->getStorage('crm_contact');
    $this->settings = $config_factory->get('crm.crm_user.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[CrmUserEvent::EVENT_NAME] = ['onCrmUserCreate', 100];

    return $events;
  }

  /**
   * React to a crm user create event.
   *
   * @param \Drupal\crm\Event\CrmUserEvent $event
   *   The crm user event.
   */
  public function onCrmUserCreate(CrmUserEvent $event) {

    $crm_user = $event->getCrmUser();
    $contact = $crm_user->getContact();
    $lookup = $this->settings->get('auto_create_lookup_contact');
    if ($lookup && $contact == NULL) {
      $contact_id = $this->storage->getQuery()
        ->condition('emails.email', $crm_user->getUser()->getEmail())
        ->accessCheck(TRUE)
        ->execute();
      if ($contact_id != NULL) {
        $contact_id = reset($contact_id);
        $contact = $this->storage
          ->load(reset($contact_id));
      }
    }
    if ($contact == NULL) {
      $contact = $this->storage
        ->create(['bundle' => 'individual']);
    }
    $contact->set('full_name', ['given' => $crm_user->getUser()->getDisplayName()]);
    $contact->set('emails', $crm_user->getUser()->getEmail());
    $contact->save();
    $crm_user->setContact($contact);
    $event->setCrmUser($crm_user);
  }

}
