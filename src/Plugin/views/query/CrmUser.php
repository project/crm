<?php

namespace Drupal\crm\Plugin\views\query;

use Drupal\views\Plugin\views\query\QueryPluginBase;

/**
 * The.
 *
 * @ViewsQuery(
 *   id = "crm_user",
 *   title = @Translation("User Contact"),
 *   help = @Translation("Relationship between the user and Individual.")
 * )
 */
class CrmUser extends QueryPluginBase {

  /**
   * {@inheritdoc}
   */
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function addField($table, $field, $alias = '', $params = []) {
    return $field;
  }

}
