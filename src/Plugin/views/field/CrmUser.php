<?php

namespace Drupal\crm\Plugin\views\field;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Link;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\crm\CrmUserSyncRelationInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contact User field plugin.
 *
 * @ViewsField("crm_user")
 */
class CrmUser extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The crm core user sync relation service.
   *
   * @var \Drupal\crm\CrmCoreUserSyncRelation
   */
  protected $service;

  /**
   * The current path service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Constructs a StringFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\crm\CrmUserSyncRelationInterface $crm_user
   *   The crm user service.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    CrmUserSyncRelationInterface $crm_user,
    CurrentPathStack $current_path,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->service = $crm_user;
    $this->currentPath = $current_path;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('crm.user'),
      $container->get('path.current')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $entity = $values->_entity;

    $entity_type = $entity->getEntityTypeId();
    $entity_id = $entity->id();

    $current_path = $this->currentPath->getPath();
    if ($entity_type == 'crm_contact') {
      $uid = $this->service->getUserIdFromIndividualId($entity_id);
      if (empty($uid)) {
        $url = Url::fromRoute(
          'entity.crm_user.add_form',
          ['crm_contact' => $entity_id],
          [
            'query' => [
              'destination' => Url::fromUri("internal:$current_path")->toString(),
            ],
          ]
        );
        $link = Link::fromTextAndUrl('Add Relation', $url)->toString();
      }
      else {
        $rid = $this->service->getRelationIdFromIndividualId($entity_id);
        $url = Url::fromRoute(
          'entity.crm_user.edit_form',
          ['crm_user' => $rid],
          [
            'query' => [
              'destination' => Url::fromUri("internal:$current_path")->toString(),
            ],
          ]
        );
        $link = Link::fromTextAndUrl($rid, $url)->toString();
      }

    }
    elseif ($entity_type == 'user') {
      $individual_id = $this->service->getContactIdFromUserId($entity_id);
      if (empty($individual_id)) {
        $url = Url::fromRoute(
          'entity.crm_user.add_form',
          ['user' => $entity_id],
          [
            'query' => [
              'destination' => Url::fromUri("internal:$current_path")->toString(),
            ],
          ]
        );
        $link = Link::fromTextAndUrl('Add Relation', $url)->toString();
      }
      else {
        $rid = $this->service->getRelationIdFromUserId($entity_id);
        $url = Url::fromRoute(
          'entity.crm_user.edit_form',
          ['crm_user' => $rid],
          [
            'query' => [
              'destination' => Url::fromUri("internal:$current_path")->toString(),
            ],
          ]
        );
        $link = Link::fromTextAndUrl($rid, $url)->toString();
      }
    }

    return $link;
  }

}
