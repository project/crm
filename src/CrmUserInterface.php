<?php

namespace Drupal\crm;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a relation entity type.
 */
interface CrmUserInterface extends ContentEntityInterface {

  /**
   * Returns the relation user entity.
   *
   * @return \Drupal\user\UserInterface
   *   The relation user entity.
   */
  public function getUser();

  /**
   * Sets the relation user entity.
   *
   * @param \Drupal\user\UserInterface $account
   *   The relation user entity.
   *
   * @return $this
   */
  public function setUser(UserInterface $account);

  /**
   * Returns the relation user ID.
   *
   * @return int|null
   *   The relation user ID, or NULL in case the user ID field has not been set.
   */
  public function getUserId();

  /**
   * Sets the relation user ID.
   *
   * @param int $uid
   *   The relation user id.
   *
   * @return $this
   */
  public function setUserId($uid);

  /**
   * Returns the relation individual entity.
   *
   * @return \Drupal\crm\CrmContactInterface
   *   The relation individual entity.
   */
  public function getContact();

  /**
   * Sets the relation individual entity.
   *
   * @param \Drupal\crm\CrmContactInterface $individual
   *   The relation individual entity.
   *
   * @return $this
   */
  public function setContact(CrmContactInterface $individual);

  /**
   * Returns the relation individual ID.
   *
   * @return int|null
   *   The relation individual ID, or NULL in case the individual ID field has
   *   not been set.
   */
  public function getContactId();

  /**
   * Sets the relation individual ID.
   *
   * @param int $individual_id
   *   The relation individual id.
   *
   * @return $this
   */
  public function setContactId($individual_id);

}
