<?php

namespace Drupal\crm\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the relation entity edit forms.
 */
class UserForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $crm_contact_id = $this->getRequest()->query->get('crm_contact');
    $user_id = $this->getRequest()->query->get('user');
    if ($crm_contact_id || $user_id) {
      $relation = $this->getEntity();
      $relation->setContactId($crm_contact_id);
      $relation->setUserId($user_id);
      $this->setEntity($relation);
    }

    $form = parent::form($form, $form_state);

    if ($crm_contact_id) {
      $form['crm_contact']['widget']['#disabled'] = TRUE;
    }

    if ($user_id) {
      $form['user']['widget']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toString();

    $logger_arguments = ['link' => $link];

    if ($result == SAVED_NEW) {
      $this->messenger()->addMessage($this->t('New relation has been created.'));
      $this->logger('crm')->notice('Created new relation', $logger_arguments);
    }
    else {
      $this->messenger()->addMessage($this->t('The relation has been updated.'));
      $this->logger('crm')->notice('Relation updated', $logger_arguments);
    }

    $form_state->setRedirect('entity.crm_user.collection');

    return $result;
  }

}
