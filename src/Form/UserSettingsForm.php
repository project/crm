<?php

namespace Drupal\crm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * CRM User settings form.
 */
class UserSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'crm_user_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'crm.crm_user.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $settings = $this->config('crm.crm_user.settings');
    $form['display_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display name'),
      '#default_value' => $settings->get('display_name'),
      '#description' => $this->t('Override the User name with the CRM name.'),
    ];

    $form['create_event'] = [
      '#type' => 'details',
      '#title' => $this->t('Create event'),
      '#open' => TRUE,
    ];

    $form['create_event']['auto_create_crm_user'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create CRM user automatically'),
      '#default_value' => $settings->get('auto_create_crm_user'),
      '#description' => $this->t('Automatically create a contact when a user is created.'),
    ];

    $form['create_event']['auto_create_lookup_contact'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lookup contact automatically'),
      '#default_value' => $settings->get('auto_create_lookup_contact'),
      '#description' => $this->t('Automatically create a lookup contact when a user is created.'),
      '#states' => [
        'enabled' => [
          ':input[name="auto_create_crm_user"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('crm.crm_user.settings')
      ->set('display_name', $form_state->getValue('display_name'))
      ->set('auto_create_crm_user', $form_state->getValue('auto_create_crm_user'))
      ->set('auto_create_lookup_contact', $form_state->getValue('auto_create_lookup_contact'))
      ->save();

    return parent::submitForm($form, $form_state);
  }

}
