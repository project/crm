<?php

namespace Drupal\crm\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the contact entity edit forms.
 */
class ContactForm extends ContentEntityForm {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a NodeForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface|null $entity_type_bundle_info
   *   The entity type bundle service.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    TimeInterface $time,
    DateFormatterInterface $date_formatter,
    AccountInterface $current_user,
    ?EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
  ) {

    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->dateFormatter = $date_formatter;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  final public static function create(ContainerInterface $container) {
    return new self(
      $container->get('entity.repository'),
      $container->get('datetime.time'),
      $container->get('date.formatter'),
      $container->get('current_user'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\crm\CrmContactInterface $contact */
    $contact = $this->entity;

    $form = parent::form($form, $form_state);

    $this->doPrimaryField($form);

    $form['advanced']['#attributes']['class'][] = 'entity-meta';

    $form['meta'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#weight' => -10,
      '#title' => $this->t('Status'),
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
      '#access' => $this->currentUser->hasPermission('administer nodes'),
    ];
    $form['meta']['published'] = [
      '#type' => 'item',
      '#markup' => $contact->isPublished() ? $this->t('Published') : $this->t('Not published'),
      '#access' => !$contact->isNew(),
      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
    ];
    $form['meta']['changed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last saved'),
      '#markup' => !$contact->isNew() ? $this->dateFormatter->format($contact->getChangedTime(), 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
    ];
    $form['meta']['author'] = [
      '#type' => 'item',
      '#title' => $this->t('Author'),
      '#markup' => $contact->getOwner()->getAccountName(),
      '#wrapper_attributes' => ['class' => ['entity-meta__author']],
    ];

    $form['status']['#group'] = 'footer';

    // Contact author information for administrators.
    $form['author'] = [
      '#type' => 'details',
      '#title' => $this->t('Authoring information'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['crm-contact-form-owner'],
      ],
      '#attached' => [
        // 'library' => ['node/drupal.node'],
      ],
      '#weight' => 90,
      '#optional' => TRUE,
    ];

    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'author';
    }

    if (isset($form['created'])) {
      $form['created']['#group'] = 'author';
    }

    // Add a custom submit handler to the entity form.
    $form['revision']['#default_value'] = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $contact = $this->getEntity();
    // Set new Revision.
    $contact->setNewRevision(TRUE);

    $result = parent::save($form, $form_state);
    $this->doPrimaryRelationship();

    $message_arguments = ['%label' => $contact->toLink()->toString()];
    $logger_arguments = [
      '%label' => $contact->label(),
      'link' => $contact->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New contact %label has been created.', $message_arguments));
        $this->logger('crm_contact')->notice('Created new contact %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The contact %label has been updated.', $message_arguments));
        $this->logger('crm_contact')->notice('Updated contact %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.crm_contact.canonical', [
      'crm_contact' => $contact->id(),
    ]);

    if ($contact->id()) {
      $form_state->setValue('id', $contact->id());
      $form_state->set('id', $contact->id());
      if ($contact->access('view')) {
        $form_state->setRedirect(
          'entity.crm_contact.canonical',
          ['crm_contact' => $contact->id()]
        );
      }
      else {
        $form_state->setRedirect('<front>');
      }

    }

    return $result;
  }

  /**
   * Ensures there is a relationship for the primary contact.
   */
  private function doPrimaryRelationship() {
    $contact = $this->getEntity();
    $primary = $contact->get('primary')->entity;
    $relationship_type_id = $contact->get('bundle')->entity->get('primary');
    if (!$primary || !$relationship_type_id) {
      return;
    }

    $relationship_type = $this->entityTypeManager->getStorage('crm_relationship_type')->load($relationship_type_id);
    $bundle = $contact->get('bundle')->target_id;

    $is_a = $relationship_type->get('contact_type_a') == $bundle;

    $storage = $this->entityTypeManager->getStorage('crm_relationship');
    $query = $storage->getQuery();
    $query->condition('bundle', $relationship_type_id);
    if ($is_a) {
      $query->condition('contact_a', $contact->id());
      $query->condition('contact_b', $primary->id());
    }
    else {
      $query->condition('contact_a', $primary->id());
      $query->condition('contact_b', $contact->id());
    }
    $has_relationship = $query->accessCheck(FALSE)->count()->execute();
    if (!$has_relationship) {
      $storage->create([
        'bundle' => $relationship_type_id,
        'contact_a' => $contact->id(),
        'contact_b' => $primary->id(),
        'start' => $this->time->getRequestTime(),
      ])->save();
    }

  }

  /**
   * Bundle specific changes to the primary field.
   *
   * @param array $form
   *   The form array.
   */
  private function doPrimaryField(&$form) {
    if (!isset($form['primary'])) {
      return;
    }
    $entity = $this->getEntity();
    $relationship_type_id = $entity->get('bundle')->entity->get('primary');
    if (!$relationship_type_id) {
      return;
    }
    $relationship_type = $this->entityTypeManager->getStorage('crm_relationship_type')->load($relationship_type_id);
    $bundle = $entity->get('bundle')->target_id;
    $is_a = $relationship_type->get('contact_type_a') == $bundle;
    $target_id = &$form['primary']['widget'][0]['target_id'];
    if ($is_a) {
      $target_bundles = [$relationship_type->get('contact_type_b')];
      $target_id['#title'] = $relationship_type->get('label_b_a');
    }
    else {
      $target_bundles = [$relationship_type->get('contact_type_a')];
      $target_id['#title'] = $relationship_type->label();
    }

    $target_id['#selection_settings']['target_bundles'] = $target_bundles;

    $target_id['#description'] = $relationship_type->get('description');
  }

}
