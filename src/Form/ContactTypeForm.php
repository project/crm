<?php

namespace Drupal\crm\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for contact type forms.
 */
class ContactTypeForm extends BundleEntityFormBase {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Constructs a ContactTypeForm object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage.
   */
  public function __construct(EntityStorageInterface $storage) {
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  final public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('crm_relationship_type'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity_type = $this->entity;

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $entity_type->label(),
      '#description' => $this->t('The human-readable name of this contact type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\crm\Entity\ContactType', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this contact type. It must only contain lowercase letters, numbers, and underscores.'),
    ];
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit %label contact type', ['%label' => $entity_type->label()]);

      $form['primary'] = [
        '#type' => 'select',
        '#title' => $this->t('Primary Relationship'),
        '#options' => $this->getRelationshipTypeOptions(),
        '#description' => $this->t('The primary relationship type for this contact type.'),
        '#default_value' => $entity_type->get('primary'),
      ];
    }

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save contact type');
    $actions['delete']['#value'] = $this->t('Delete contact type');

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\crm\Entity\ContactType $entity_type */
    $entity_type = $this->entity;

    $entity_type->set('id', trim($entity_type->id()));
    $entity_type->set('label', trim($entity_type->label()));
    if ($this->operation == 'edit') {
      $entity_type->set('primary', trim($entity_type->get('primary')));
    }

    $status = parent::save($form, $form_state);
    $t_args = ['%name' => $entity_type->label()];
    if ($status == SAVED_UPDATED) {
      $message = $this->t('The contact type %name has been updated.', $t_args);
      $this->messenger()->addStatus($message);
    }
    elseif ($status == SAVED_NEW) {
      $message = $this->t('The contact type %name has been added.', $t_args);
      $this->messenger()->addStatus($message);
    }

    $form_state->setRedirectUrl($entity_type->toUrl('collection'));

    return $status;
  }

  /**
   * Get the relationship type options.
   *
   * @return array
   *   An array of relationship type options.
   */
  private function getRelationshipTypeOptions() {

    $query = $this->storage->getQuery();
    $group = $query->orConditionGroup()
      ->condition('contact_type_a', $this->entity->id())
      ->condition('contact_type_b', $this->entity->id());
    $query->condition($group);
    $relationship_type_ids = $query->accessCheck(FALSE)->execute();
    $relationship_types = $this->storage->loadMultiple($relationship_type_ids);
    $options = [];
    foreach ($relationship_types as $relationship_type) {
      $is_a = $relationship_type->get('contact_type_a') == $this->entity->id();
      if ($is_a) {
        $options[$relationship_type->id()] = $relationship_type->label();
      }
      else {
        $options[$relationship_type->id()] = $relationship_type->get('label_b_a');
      }

    }
    return $options;
  }

}
