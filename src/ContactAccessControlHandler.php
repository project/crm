<?php

namespace Drupal\crm;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the contact entity type.
 */
class ContactAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected $viewLabelOperation = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view label':
        $permissions = ['view any crm_contact label'];

        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');

      case 'view':
        $permissions = ['view any crm_contact'];

        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');

      case 'update':
        $permissions = ['edit any crm_contact'];
        if ($this->isOwner($entity, $account)) {
          $permissions[] = 'edit own crm_contact';
        }
        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR',
        );

      case 'delete':
        $permissions = ['delete any crm_contact'];
        if ($this->isOwner($entity, $account)) {
          $permissions[] = 'delete own crm_contact';
        }
        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR',
        );

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
      $account,
      ['create crm_contact', 'administer contact'],
      'OR',
    );
  }

  /**
   * Is the given entity owned by the given account?
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check.
   *
   * @return bool
   *   TRUE if the entity is owned by the account, FALSE otherwise.
   */
  protected function isOwner(EntityInterface $entity, AccountInterface $account): bool {
    return $entity->getOwnerId() === $account->id();
  }

}
