<?php

namespace Drupal\crm;

use Drupal\user\UserInterface;

/**
 * CrmCoreUserSyncRelation service.
 */
interface CrmUserSyncRelationInterface {

  /**
   * Retrieves the individual ID from the user ID.
   *
   * @return int|null
   *   Individual ID, if relation exists.
   */
  public function getContactIdFromUserId($user_id);

  /**
   * Retrieves the user ID from the individual ID.
   *
   * @return int|null
   *   User ID, if relation exists.
   */
  public function getUserIdFromIndividualId($individual_id);

  /**
   * Retrieves the relation ID from the user ID.
   *
   * @return int|null
   *   Relation ID, if exists.
   */
  public function getRelationIdFromUserId($user_id);

  /**
   * Retrieves the relation ID from the individual ID.
   *
   * @return int|null
   *   Relation ID, if exists.
   */
  public function getRelationIdFromIndividualId($individual_id);

  /**
   * Synchronizes user and contact.
   *
   * @param \Drupal\user\UserInterface $account
   *   Account to be synchronized. Programmatically created accounts can
   *   override default behavior by setting
   *   $account->crm_core_no_auto_sync = TRUE.
   * @param \Drupal\crm\CrmContactInterface $individual
   *   Contact to be associated with $account.
   *
   * @return \Drupal\crm\ContactInterface
   *   A contact object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function relate(UserInterface $account, ?CrmContactInterface $individual = NULL);

}
