<?php

namespace Drupal\crm_field\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'crm_email' field widget.
 *
 * @FieldWidget(
 *   id = "crm_email",
 *   label = @Translation("CRM Email"),
 *   field_types = {"crm_email"},
 * )
 */
class EmailWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['foo' => 'bar'] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $element['foo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Foo'),
      '#default_value' => $settings['foo'],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary[] = $this->t('Foo: @foo', ['@foo' => $settings['foo']]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    $element['primary'] = [
      '#type' => 'radio',
      '#return_value' => $delta,
      '#title' => $this->t('Primary'),
      '#default_value' => $items[$delta]->primary ? $delta : NULL,
      '#attributes' => [
        'id' => $field_name . '-primary-' . $delta,
      ],
      '#parents' => [$field_name, 0, 'primary'],
    ];

    $element['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => $items[$delta]->email ?? NULL,
    ];

    $element['location_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Location'),
      '#default_value' => $items[$delta]->location_id ?? NULL,
      '#options' => $this->getLocations(),
    ];

    $element['hold'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hold'),
      '#default_value' => $items[$delta]->hold ?? NULL,
    ];

    $element['bulk'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bulk'),
      '#default_value' => $items[$delta]->bulk ?? NULL,
    ];

    $element['hold_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Hold Date'),
      '#default_value' => NULL,
      '#state' => [
        'visible' => [
          ':input[name="field_email[' . $delta . '][hold]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    if (isset($items[$delta]->hold_date)) {
      $element['hold_date']['#default_value'] = DrupalDateTime::createFromFormat(
        'Y-m-d\TH:i:s',
        $items[$delta]->hold_date,
        'UTC'
      );
    }

    $element['reset_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Reset Date'),
      '#default_value' => NULL,
    ];
    if (isset($items[$delta]->reset_date)) {
      $element['reset_date']['#default_value'] = DrupalDateTime::createFromFormat(
        'Y-m-d\TH:i:s',
        $items[$delta]->reset_date,
        'UTC'
      );
    }

    $element['#theme_wrappers'] = ['container', 'form_element'];
    $element['#attributes']['class'][] = 'crm-email-elements';
    $element['#attached']['library'][] = 'crm_field/crm_email';

    return $element;
  }

  /**
   * Get locations.
   *
   * @return array
   *   Locations.
   */
  protected function getLocations() {
    $locations = [];
    $locations['home'] = $this->t('Home');
    $locations['work'] = $this->t('Work');
    $locations['other'] = $this->t('Other');

    return $locations;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return isset($violation->arrayPropertyPath[0]) ? $element[$violation->arrayPropertyPath[0]] : $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $primary = 0;
    foreach ($values as $delta => $value) {
      if (isset($value['primary'])) {
        $primary = $value['primary'];
      }
    }
    foreach ($values as $delta => $value) {
      $values[$delta]['primary'] = FALSE;
      if ($values[$delta]['_original_delta'] == $primary) {
        $values[$delta]['primary'] = TRUE;
      }

      if ($value['email'] === '') {
        $values[$delta]['email'] = NULL;
      }
      if ($value['location_id'] === '') {
        $values[$delta]['location_id'] = NULL;
      }
      if ($value['hold'] === '') {
        $values[$delta]['hold'] = NULL;
      }
      if ($value['bulk'] === '') {
        $values[$delta]['bulk'] = NULL;
      }
      if ($value['hold_date'] === '') {
        $values[$delta]['hold_date'] = NULL;
      }
      if ($value['hold_date'] instanceof DrupalDateTime) {
        $values[$delta]['hold_date'] = $value['hold_date']->format('Y-m-d\TH:i:s');
      }
      if ($value['reset_date'] === '') {
        $values[$delta]['reset_date'] = NULL;
      }
      if ($value['reset_date'] instanceof DrupalDateTime) {
        $values[$delta]['reset_date'] = $value['reset_date']->format('Y-m-d\TH:i:s');
      }
    }

    usort($values, function ($a, $b) {
      if ($a['primary'] == $b['primary']) {
        return 0;
      }
      return ($a['primary'] < $b['primary']) ? 1 : -1;
    });

    return $values;
  }

}
