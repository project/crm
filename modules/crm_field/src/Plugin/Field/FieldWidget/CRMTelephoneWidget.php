<?php

namespace Drupal\crm_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\crm_field\Plugin\Field\FieldType\CRMTelephoneItem;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'crm_telephone' field widget.
 *
 * @FieldWidget(
 *   id = "crm_telephone",
 *   label = @Translation("CRM Telephone"),
 *   field_types = {"crm_telephone"},
 * )
 */
class CRMTelephoneWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['foo' => 'bar'] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $element['foo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Foo'),
      '#default_value' => $settings['foo'],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary[] = $this->t('Foo: @foo', ['@foo' => $settings['foo']]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    $element['primary'] = [
      '#type' => 'radio',
      '#return_value' => $delta,
      '#title' => $this->t('Primary'),
      '#default_value' => $items[$delta]->primary ? $delta : NULL,
      '#attributes' => [
        'id' => $field_name . '-primary-' . $delta,
      ],
      '#parents' => [$field_name, 0, 'primary'],
    ];

    $element['location_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Location'),
      '#options' => CRMTelephoneItem::allowedLocationValues(),
      '#default_value' => $items[$delta]->location_id ?? 'main',
      '#required' => TRUE,
    ];

    $element['phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone'),
      '#default_value' => $items[$delta]->phone ?? NULL,
      '#attributes' => [
        'size' => 10,
      ],
    ];

    $element['phone_ext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Extension'),
      '#size' => 5,
      '#default_value' => $items[$delta]->phone_ext ?? NULL,
    ];

    $element['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => CRMTelephoneItem::allowedTypeValues(),
      '#default_value' => $items[$delta]->type ?? 'phone',
      '#required' => TRUE,
    ];

    $element['mobile_provider_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Mobile Provider'),
      '#options' => ['' => $this->t('- None -')] + CRMTelephoneItem::allowedMobileProviderValues(),
      '#default_value' => $items[$delta]->mobile_provider_id ?? NULL,
      '#states' => [
        'visible' => [
          ':input[name="' . $field_name . '[' . $delta . '][type]"]' => ['value' => 'mobile'],
        ],
      ],
    ];

    $element['#theme_wrappers'] = ['container', 'form_element'];
    $element['#attributes']['class'][] = 'crm-telephone-elements';
    $element['#attached']['library'][] = 'crm_field/crm_telephone';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return isset($violation->arrayPropertyPath[0]) ? $element[$violation->arrayPropertyPath[0]] : $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $primary = 0;
    foreach ($values as $delta => $value) {
      if (isset($value['primary'])) {
        $primary = $value['primary'];
      }
    }
    foreach ($values as $delta => $value) {
      $values[$delta]['primary'] = FALSE;
      if ($values[$delta]['_original_delta'] == $primary) {
        $values[$delta]['primary'] = TRUE;
      }

      if ($value['phone'] === '') {
        $values[$delta]['phone'] = NULL;
      }
      if ($value['phone_ext'] === '') {
        $values[$delta]['phone_ext'] = NULL;
      }
      if ($value['type'] === '') {
        $values[$delta]['type'] = NULL;
      }
      if ($value['location_id'] === '') {
        $values[$delta]['location_id'] = NULL;
      }
      if ($value['mobile_provider_id'] === '') {
        $values[$delta]['mobile_provider_id'] = NULL;
      }
    }

    usort($values, function ($a, $b) {
      if ($a['primary'] == $b['primary']) {
        return 0;
      }
      return ($a['primary'] < $b['primary']) ? 1 : -1;
    });

    return $values;
  }

}
