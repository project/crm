<?php

namespace Drupal\crm_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'crm_email_default' formatter.
 *
 * @FieldFormatter(
 *   id = "crm_email_default",
 *   label = @Translation("Default"),
 *   field_types = {"crm_email"}
 * )
 */
class EmailDefaultFormatter extends FormatterBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    DateFormatterInterface $date_formatter,
  ) {
    parent::__construct($plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings,
    );

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  final public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {

      $element[$delta]['email'] = [
        '#type' => 'item',
        '#title' => $this->t('Email'),
        'content' => [
          '#type' => 'link',
          '#title' => $item->email,
          '#url' => Url::fromUri('mailto:' . $item->email),
        ],
      ];

      if ($item->location_id) {
        $element[$delta]['location_id'] = [
          '#type' => 'item',
          '#title' => $this->t('Location'),
          '#markup' => $item->location_id,
        ];
      }

      if ($item->primary) {
        $element[$delta]['primary'] = [
          '#type' => 'item',
          '#title' => $this->t('Primary'),
          '#markup' => $item->primary ? $this->t('Yes') : $this->t('No'),
        ];
      }

      if ($item->hold) {
        $element[$delta]['hold'] = [
          '#type' => 'item',
          '#title' => $this->t('Hold'),
          '#markup' => $item->hold ? $this->t('Yes') : $this->t('No'),
        ];
      }

      if ($item->bulk) {
        $element[$delta]['bulk'] = [
          '#type' => 'item',
          '#title' => $this->t('Bulk'),
          '#markup' => $item->bulk ? $this->t('Yes') : $this->t('No'),
        ];
      }

      if ($item->hold_date) {
        $date = DrupalDateTime::createFromFormat('Y-m-d\TH:i:s', $item->hold_date);

        $timestamp = $date->getTimestamp();
        $formatted_date = $this->dateFormatter->format($timestamp, 'long');
        $iso_date = $this->dateFormatter->format($timestamp, 'custom', 'Y-m-d\TH:i:s') . 'Z';
        $element[$delta]['hold_date'] = [
          '#type' => 'item',
          '#title' => $this->t('Hold Date'),
          'content' => [
            '#theme' => 'time',
            '#text' => $formatted_date,
            '#html' => FALSE,
            '#attributes' => [
              'datetime' => $iso_date,
            ],
            '#cache' => [
              'contexts' => [
                'timezone',
              ],
            ],
          ],
        ];
      }

      if ($item->reset_date) {
        $date = DrupalDateTime::createFromFormat('Y-m-d\TH:i:s', $item->reset_date);

        $timestamp = $date->getTimestamp();
        $formatted_date = $this->dateFormatter->format($timestamp, 'long');
        $iso_date = $this->dateFormatter->format($timestamp, 'custom', 'Y-m-d\TH:i:s') . 'Z';
        $element[$delta]['reset_date'] = [
          '#type' => 'item',
          '#title' => $this->t('Reset Date'),
          'content' => [
            '#theme' => 'time',
            '#text' => $formatted_date,
            '#html' => FALSE,
            '#attributes' => [
              'datetime' => $iso_date,
            ],
            '#cache' => [
              'contexts' => [
                'timezone',
              ],
            ],
          ],
        ];
      }

    }

    return $element;
  }

}
