<?php

namespace Drupal\crm_case_external_id\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueInteger constraint.
 */
class ExternalIdConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Creates a new TaxonomyTermHierarchyConstraintValidator instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $entity = $value->getEntity();
    $field_definition = $value->getFieldDefinition();

    foreach ($value as $item) {
      // First check if the value is an integer.
      if (!is_int((int) $item->value)) {
        // The value is not an integer, so a violation, aka error, is applied.
        // The type of violation applied comes from the constraint description
        // in step 1.
        $this->context->addViolation($constraint->notInteger, ['%value' => $item->value]);
      }

      // Next check if the value is unique.
      if (!$this->isUnique($item->value, $entity, $field_definition)) {
        $this->context->addViolation($constraint->notUnique, ['%value' => $item->value]);
      }
    }
  }

  /**
   * Is unique?
   *
   * @param string $value
   *   The value to check.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   */
  private function isUnique($value, $entity, $field_definition) {

    $entity_type = $field_definition->get('entity_type');
    $field_name = $field_definition->get('field_name');
    $bundle = $field_definition->get('bundle');
    $bundles = $field_definition->getThirdPartySetting('external_id', 'bundles', []);
    $bundles = array_filter($bundles);
    $negate = $field_definition->getThirdPartySetting('external_id', 'negate', FALSE);
    // Here is where the check for a unique value would happen.
    $query = $this->entityTypeManager->getStorage($entity_type)->getQuery();
    $query->condition($field_name, $value);
    if (!$entity->isNew()) {
      $entity_id = $entity->id();
      $query->condition('id', $entity_id, '<>');
    }
    if (!empty($bundles)) {
      $operator = $negate ? 'NOT IN' : 'IN';
      $query->condition('bundle', $bundles, $operator);
    }

    $results = $query->accessCheck(FALSE)->count()->execute();

    return $results === 0;
  }

}
