<?php

namespace Drupal\crm_case_external_id\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "CrmCaseExternalId",
 *   label = @Translation("External Crm Case Id", context = "Validation"),
 *   type = "integer"
 * )
 */
class ExternalIdConstraint extends Constraint {

  /**
   * The message that will be shown if the value is not an integer.
   *
   * @var string
   */
  public $notInteger = '%value is not an integer';

  /**
   * The message that will be shown if the value is not unique.
   *
   * @var string
   */
  public $notUnique = '%value is not unique';

}
