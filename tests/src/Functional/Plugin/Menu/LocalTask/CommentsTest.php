<?php

namespace Drupal\Tests\crm\Functional\Plugin\Menu\LocalTask;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Tests\crm\Functional\ContactTestBase;

/**
 * Tests the Comments local task plugin.
 *
 * @group crm
 */
class CommentsTest extends ContactTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crm', 'comment', 'block'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * Test that the local task title displays 'Notes (3)'.
   */
  public function testLocalTaskTitle() {
    // Create a new CRM contact entity.
    $contact = $this->createContact();

    // Create some comments on the contact entity.
    $this->createComment($contact, 'Note 1');
    $this->createComment($contact, 'Note 2');
    $this->createComment($contact, 'Note 3');

    $user = $this->drupalCreateUser([
      'view any crm_contact',
      'access comments',
    ]);

    $this->drupalLogin($user);

    // Visit the contact page.
    $this->drupalGet('/crm/contact/' . $contact->id());

    // Check that the Comments local task displays the correct number of notes.
    $this->assertSession()->pageTextContains('Notes (3)');
  }

  /**
   * Test that the local task title displays 'Note (1)'.
   */
  public function testLocalTaskTitleSingle() {
    // Create a new CRM contact entity.
    $contact = $this->createContact();

    // Create some comments on the contact entity.
    $this->createComment($contact, 'Note 1');

    $user = $this->drupalCreateUser([
      'view any crm_contact',
      'access comments',
    ]);

    $this->drupalLogin($user);

    // Visit the contact page.
    $this->drupalGet('/crm/contact/' . $contact->id());

    // Check that the Comments local task displays the correct number of note.
    $this->assertSession()->pageTextContains('Note (1)');
  }

  /**
   * Test that the local task title displays 'Notes (0)'.
   */
  public function testLocalTaskTitleNone() {
    // Create a new CRM contact entity.
    $contact = $this->createContact();

    $user = $this->drupalCreateUser([
      'view any crm_contact',
      'access comments',
    ]);

    $this->drupalLogin($user);

    // Visit the contact page.
    $this->drupalGet('/crm/contact/' . $contact->id());

    // Check that the Comments local task displays the correct number of notes.
    $this->assertSession()->pageTextContains('Notes (0)');
  }

  /**
   * Helper method to create a new CRM contact entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The newly created entity.
   */
  protected function createContact() {
    $values = [
      'name' => $this->randomString(),
      'bundle' => 'organization',
    ];
    $contact = $this->container
      ->get('entity_type.manager')
      ->getStorage('crm_contact')
      ->create($values);
    $contact->save();

    return $contact;
  }

  /**
   * Helper method to create a new comment on a CRM contact entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $contact
   *   The CRM contact entity.
   * @param string $text
   *   The comment text.
   */
  protected function createComment(EntityInterface $contact, $text) {
    $values = [
      'entity_type' => 'crm_contact',
      'entity_id' => $contact->id(),
      'comment_type' => 'contact_comment',
      'subject' => $text,
      'field_name' => 'field_notes',
      'status' => 1,
    ];
    $comment = $this->container->get('entity_type.manager')
      ->getStorage('comment')
      ->create($values);
    $comment->save();

  }

}
