<?php

namespace Drupal\Tests\crm\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\crm\Entity\Contact;
use Drupal\crm\Entity\Relationship;

/**
 * Tests the RelationshipController.
 *
 * @group crm
 */
class RelationshipControllerTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crm'];

  /**
   * Tests the RelationshipController.
   */
  public function testRelationshipController() {

    $contact_a = Contact::create([
      'bundle' => 'Individual',
      'name' => 'Some Guy',
    ]);
    $contact_a->save();
    $a_id = $contact_a->id();
    $contact_b = Contact::create([
      'bundle' => 'organization',
      'name' => 'Test Organization',
    ]);
    $contact_b->save();

    // Create a CRM relationship entity.
    $relationship = Relationship::create([
      'contact_a' => $contact_a->id(),
      'contact_b' => $contact_b->id(),
      'bundle' => 'employee',
      'status' => 1,
      'start' => strtotime('2023-01-01'),
      'end' => strtotime('2023-12-31'),
    ]);
    $relationship->save();

    $user = $this->drupalCreateUser([
      'view any crm_contact',
      'view crm relationship',
      'edit crm relationship',
      'delete crm relationship',
      'administer crm relationship types',
    ]);

    $this->drupalLogin($user);

    // Visit the path handled by the RelationshipController.
    $this->drupalGet('crm/contact/' . $a_id . '/relationship');
    $this->assertSession()->statusCodeEquals(200);

    // Assert that the active relationship is displayed in the table.
    $this->assertSession()->responseContains('There are no inactive relationships.');
    $this->assertSession()->responseContains($relationship->bundle->entity->label());
    $this->assertSession()->responseContains($relationship->get('contact_b')->entity->label());
    $this->assertSession()->responseContains('New Lenox');
    $this->assertSession()->responseContains('IL');
    $this->assertSession()->responseContains('webmaster@openknowledge.works');
    $this->assertSession()->responseContains('815-485-0000');

    // Assert that the edit and delete links are present.
    $this->assertSession()->linkByHrefExists($relationship->toUrl('edit-form')->toString());
    $this->assertSession()->linkByHrefExists($relationship->toUrl('delete-form')->toString());

    // Update the relationship status to inactive.
    $relationship->set('status', 0)->save();

    // Visit the page again and
    // assert that the inactive relationship is displayed.
    $this->drupalGet('crm/contact/' . $a_id . '/relationship');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('There are no active relationships.');
    $this->assertSession()->responseContains($relationship->bundle->entity->label());
    $this->assertSession()->responseContains($relationship->get('contact_b')->entity->label());

    // Visit the page again and
    // assert that the inactive relationship is displayed.
    $relationship_1 = Relationship::load(1);
    $relationship_1->delete();
    $this->drupalGet('admin/content/crm/relationship');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('There are no crm relationships yet.');

  }

}
