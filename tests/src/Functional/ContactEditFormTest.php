<?php

namespace Drupal\Tests\crm\Functional;

/**
 * Create a contact and test contact edit functionality.
 *
 * @group crm
 */
class ContactEditFormTest extends ContactTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * A normal logged in user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * A user with permission to bypass content access checks.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * The contact storage.
   *
   * @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface
   */
  protected $contactStorage;

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['block', 'crm', 'datetime'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->webUser = $this->drupalCreateUser([
      'edit any crm_contact',
      'create crm_contact',
      'view any crm_contact',
    ]);
    $this->adminUser = $this->drupalCreateUser([
      'administer contact types',
      'edit any crm_contact',
      'create crm_contact',
      'view any crm_contact',
    ]);
    $this->drupalPlaceBlock('local_tasks_block');

    $this->contactStorage = $this->container->get('entity_type.manager')->getStorage('crm_contact');
  }

  /**
   * Checks contact edit functionality.
   */
  public function testContactEdit() {
    $this->drupalLogin($this->webUser);

    $name_key = 'name[0][value]';
    // Create contact to edit.
    $edit = [];
    $edit[$name_key] = $this->randomMachineName(8);
    $this->drupalGet('crm/contact/add/organization');
    $this->submitForm($edit, 'Save');

    // Check that the contact exists in the database.
    $contact = $this->drupalGetContactByName($edit[$name_key]);
    $this->assertNotEmpty($contact, 'Contact found in database.');

    // Check that "edit" link points to correct page.
    $this->clickLink('Edit');
    $this->assertSession()->addressEquals($contact->toUrl('edit-form'));

    // Check that the name and body fields display with the correct values.
    // @todo Ideally assertLink would support HTML, but it doesn't.
    $this->assertSession()->responseContains('Edit');
    $this->assertSession()->fieldValueEquals($name_key, $edit[$name_key]);

    // Edit the content of the node.
    $edit = [];
    $edit[$name_key] = $this->randomMachineName(8);
    // Stay on the current page, without reloading.
    $this->submitForm($edit, 'Save');

    // Check that the name and body fields display with the updated values.
    $this->assertSession()->pageTextContains($edit[$name_key]);

    // Log in as a second administrator user.
    $second_web_user = $this->drupalCreateUser([
      'create crm_contact',
      'view any crm_contact',
      'edit any crm_contact',
    ]);
    $this->drupalLogin($second_web_user);
    $first_revision_id = $contact->getRevisionId();
    // Edit the same contact, creating a new revision.
    $this->drupalGet("crm/contact/" . $contact->id() . "/edit");
    $edit = [];
    $edit['name[0][value]'] = $this->randomMachineName(8);
    // $edit['revision'] = 1;
    $this->submitForm($edit, 'Save');

    // Ensure that the contact revision has been created.
    $this->contactStorage->resetCache();
    $revised_contact = $this->contactStorage->load($contact->id());
    $this->assertNotSame($first_revision_id, $revised_contact->getRevisionId(), 'A new revision has been created.');
    // Ensure that the contact owner is preserved when it was not changed in the
    // edit form.
    $this->assertSame($contact->getOwnerId(), $revised_contact->getOwnerId(), 'The contact author has been preserved.');
    // Ensure that the revision owners are different since the revisions were
    // made by different users.
    /** @var \Drupal\Core\Entity\RevisionableStorageInterface $contact_storage */
    $contact_storage = \Drupal::service('entity_type.manager')->getStorage('crm_contact');
    $first_contact_version = $contact_storage->loadRevision($contact->getRevisionId());
    $second_contact_version = $contact_storage->loadRevision($revised_contact->getRevisionId());
    $this->assertNotSame($first_contact_version->getRevisionUser()->id(), $second_contact_version->getRevisionUser()->id(), 'Each revision has a distinct user.');

    // Check if the contact revision checkbox is rendered on node edit form.
    $this->drupalGet('crm/contact/' . $contact->id() . '/edit');
    $this->assertSession()->fieldExists('edit-revision', NULL);

    // Check that details form element opens when there are errors on child
    // elements.
    $this->drupalGet('crm/contact/' . $contact->id() . '/edit');
    $edit = [];
    // This invalid date will trigger an error.
    $edit['created[0][value][date]'] = $this->randomMachineName(8);
    // Get the current amount of open details elements.
    $open_details_elements = count($this->cssSelect('details[open="open"]'));
    $this->submitForm($edit, 'Save');

    // Edit the same contact, save it and verify it's unpublished after
    // unchecking the 'Published' boolean_checkbox and clicking 'Save'.
    $this->drupalGet("crm/contact/" . $contact->id() . "/edit");
    $edit = ['status[value]' => FALSE];
    $this->submitForm($edit, 'Save');
    $this->contactStorage->resetCache([$contact->id()]);
    $contact = $this->contactStorage->load($contact->id());
    $this->assertFalse($contact->isPublished(), 'Contact is unpublished');
  }

  /**
   * Tests the contact meta information.
   */
  public function testContactMetaInformation() {
    // Check that regular users (i.e. without the 'administer nodes' permission)
    // can not see the meta information.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('crm/contact/add/organization');
    $this->assertSession()->pageTextNotContains('Not saved yet');

    // Create node to edit.
    $edit['name[0][value]'] = $this->randomMachineName(8);
    $this->submitForm($edit, 'Save');

    $contact = $this->drupalGetContactByName($edit['name[0][value]']);
    $this->drupalGet("crm/contact/" . $contact->id() . "/edit");
    $this->assertSession()->pageTextNotContains('Published');
    $this->assertSession()->pageTextNotContains($this->container->get('date.formatter')->format($contact->get('changed')->value, 'short'));

    // Check that users with the 'administer nodes' permission can see the meta
    // information.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('crm/contact/add/organization');
    // $this->assertSession()->pageTextContains('Not saved yet');
    // Create node to edit.
    $edit['name[0][value]'] = $this->randomMachineName(8);
    $this->submitForm($edit, 'Save');

    $contact = $this->drupalGetContactByName($edit['name[0][value]']);
    $this->drupalGet("crm/contact/" . $contact->id() . "/edit");
    // $this->assertSession()->pageTextContains('Enabled');
  }

}
